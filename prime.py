def primeEndGreater(start, end):
    # Loop for check prime number from Start to End
    while start < end:
        isPrime: bool = True

        # Check the number is not 1, cause prime number start with 2
        if start != 1:
            # Check for number is prime or not
            for i in range(2, start):
                # Check if number is not prime
                if start % i == 0:
                    isPrime = False
                    break

            if isPrime:
                print(start, end=" ")

        start += 1


def primeStartGreater(start, end):
    # Loop for check prime number from Start to End
    while end < start:
        isPrime: bool = True

        # Check the number is not 1, cause prime number start with 2
        if end != 1:
            # Check for number is prime or not
            for i in range(2, end):
                # Check if number is not prime
                if end % i == 0:
                    isPrime = False
                    break

            if isPrime:
                print(end, end=" ")

        end += 1