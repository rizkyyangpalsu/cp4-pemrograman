from prime import primeEndGreater, primeStartGreater


def application():
    start: int = int(input("Input start of number prime number: "))
    end: int = int(input("Input end of number prime number: "))

    if start < end:
        primeEndGreater(start, end)
    else:
        primeStartGreater(start, end)

    again = input("\nAgain to use this application? Y to Yes ")
    if again == "Y":
        application()


print("Welcome to Prime Number Application", end="\n\n")
application()
